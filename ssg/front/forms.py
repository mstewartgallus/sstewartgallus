# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import django.contrib.auth
from django import forms
from django.core.validators import validate_email, RegexValidator


_minimum_username_length = 8
_maximum_username_length = 30

_minimum_password_length = 8
_maximum_password_length = 30

class SignupForm(forms.Form):
    username = forms.CharField(label = "Username",
                               min_length=_minimum_username_length,
                               max_length=_maximum_username_length,
                               validators=[RegexValidator(regex="^[a-z][a-z0-9]*$")],
                               widget=forms.TextInput(attrs={ 'required': 'required' }))
    email = forms.CharField(label = "Email",
                            max_length=150,
                            validators=[validate_email],
                            widget=forms.TextInput(attrs={ 'required': 'required' }))
    password = forms.CharField(label = "Password",
                               min_length=_minimum_password_length,
                               max_length=_maximum_password_length,
                               widget=forms.PasswordInput(attrs={ 'required': 'required' }))
    verify_password = forms.CharField(label = "Verify Password",
                                      min_length=_minimum_password_length,
                                      max_length=_maximum_password_length,
                                      widget=forms.PasswordInput(attrs={ 'required': 'required' }))

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()

        password = cleaned_data.get("password")
        verify_password = cleaned_data.get("verify_password")

        if not password:
            return cleaned_data
        if not verify_password:
            return cleaned_data

        if password != verify_password:
            raise forms.ValidationError("Password entries must be the same")

        return cleaned_data

class LoginForm(forms.Form):
    username = forms.CharField(label = "Username",
                               min_length=_minimum_username_length,
                               max_length=_maximum_username_length,
                               validators=[RegexValidator(regex="^[a-z][a-z0-9]*$")],
                               widget=forms.TextInput(attrs={ 'required': 'required' }))
    password = forms.CharField(label = "Password",
                               min_length=_minimum_password_length,
                               max_length=_maximum_password_length,
                               widget=forms.PasswordInput(attrs={ 'required': 'required' }))

    def __init__(self, *args):
        super(LoginForm, self).__init__(*args)

        self._user = None

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()

        username = cleaned_data.get("username")
        password = cleaned_data.get("password")

        if not username:
            return cleaned_data
        if not password:
            return cleaned_data

        user = django.contrib.auth.authenticate(username=username,
                                                password=password)
        if user is None:
            raise forms.ValidationError("Username or password not found")

        if not user.is_active:
            raise forms.ValidationError("Account is disabled")

        self._user = user

        return cleaned_data

    def login(self, request):
        django.contrib.auth.login(request, self._user)

class SettingsForm(forms.Form):
    twitter = forms.CharField(label = "Twitter",
                              validators=[RegexValidator(regex="^[_a-zA-Z0-9]*$")],
                              max_length=15,
                              required = False)
    facebook = forms.CharField(label = "Facebook",
                               validators=[RegexValidator(regex="^[.a-zA-Z0-9]*$")],
                               min_length=5,
                               max_length=50,
                               required = False)

    interests = forms.CharField(label = "Interests",
                                validators=[RegexValidator(regex="^([a-z0-9]+)?( [a-z0-9]+)*$")],
                                required = False)

class PasswordSettingsForm(forms.Form):
    new_password = forms.CharField(label = "New Password",
                               min_length=_minimum_password_length,
                               max_length=_maximum_password_length,
                               widget=forms.PasswordInput(attrs={ 'required': 'required' }))
    verify_new_password = forms.CharField(label = "Verify New Password",
                                      min_length=_minimum_password_length,
                                      max_length=_maximum_password_length,
                                      widget=forms.PasswordInput(attrs={ 'required': 'required' }))

    old_password = forms.CharField(label = "Old Password",
                                      min_length=_minimum_password_length,
                                      max_length=_maximum_password_length,
                                      widget=forms.PasswordInput(attrs={ 'required': 'required' }))

    def clean(self):
        cleaned_data = super(PasswordSettingsForm, self).clean()

        new_password = cleaned_data.get("new_password")
        verify_new_password = cleaned_data.get("verify_new_password")
        old_password = cleaned_data.get("old_password")

        if not new_password:
            return cleaned_data
        if not verify_new_password:
            return cleaned_data

        if new_password != verify_new_password:
            raise forms.ValidationError("Password entries must be the same")

        return cleaned_data
